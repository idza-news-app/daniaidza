package com.dania.danianews.api

import com.dania.danianews.model.MostResponse
import com.dania.danianews.model.SearchResponse
import com.dania.danianews.model.TopStoriesResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    companion object {
        private const val BASE_URL = "https://api.nytimes.com/"
        private const val ENDP_TOP_STORIES = BASE_URL + "svc/topstories/v2/world.json"
        private const val ENDP_MOST_VIEWED = BASE_URL + "svc/mostpopular/v2/viewed/1.json"
        private const val ENDP_MOST_SHARED = BASE_URL + "svc/mostpopular/v2/shared/1/facebook.json"
        private const val ENDP_MOST_EMAILED = BASE_URL + "svc/mostpopular/v2/emailed/7.json"
        private const val ENDP_SEARCH = BASE_URL + "svc/search/v2/articlesearch.json"

        var retrofitService: RetrofitService? = null
        fun getInstance() : RetrofitService {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }

    @GET(ENDP_TOP_STORIES)
    suspend fun getTopStory(@Query("api-key") apiKey: String?) : Response<TopStoriesResponse>

    @GET(ENDP_MOST_VIEWED)
    suspend fun getMostViewed(@Query("api-key") apiKey: String?) : Response<MostResponse>

    @GET(ENDP_MOST_SHARED)
    suspend fun getMostShared(@Query("api-key") apiKey: String?) : Response<MostResponse>

    @GET(ENDP_MOST_EMAILED)
    suspend fun getMostEmailed(@Query("api-key") apiKey: String?) : Response<MostResponse>

    @GET(ENDP_SEARCH)
    suspend fun search(@Query("q") search: String?, @Query("api-key") apiKey: String?) : Response<SearchResponse>
}