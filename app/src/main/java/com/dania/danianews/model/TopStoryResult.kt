package com.dania.danianews.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TopStoryResult(
    @SerializedName("title")
    @Expose
    var title: String? = null,
    @SerializedName("multimedia")
    @Expose
    var multimedia: Array<Multimedia>? = null
)
