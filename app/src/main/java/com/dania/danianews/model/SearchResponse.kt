package com.dania.danianews.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("status")
    @Expose
    var status: String? = null,
    @SerializedName("copyright")
    @Expose
    var copyright: String? = null,
    @SerializedName("response")
    @Expose
    var response: ResponseDoc? = null
)
