package com.dania.danianews.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MediaResult(
    @SerializedName("media-metadata")
    @Expose
    var mediaMetadata: Array<MediaMetadata>? = null
)
