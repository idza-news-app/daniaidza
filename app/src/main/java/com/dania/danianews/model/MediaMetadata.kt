package com.dania.danianews.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MediaMetadata(
    @SerializedName("url")
    @Expose
    var url: String? = null,
)
