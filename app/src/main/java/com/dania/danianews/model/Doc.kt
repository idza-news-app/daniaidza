package com.dania.danianews.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Doc(
    @SerializedName("pub_date")
    @Expose
    var pubDate: String? = null,
    @SerializedName("headline")
    @Expose
    var headline: Headline? = null,
    @SerializedName("multimedia")
    @Expose
    var multimedia: Array<Multimedia>? = null
)
