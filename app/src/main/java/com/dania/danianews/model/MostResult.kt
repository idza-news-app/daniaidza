package com.dania.danianews.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class MostResult(
    @SerializedName("title")
    @Expose
    var title: String? = null,
    @SerializedName("published_date")
    @Expose
    var published_date: String? = null,
    @SerializedName("media")
    @Expose
    var media: Array<MediaResult>? = null
)
