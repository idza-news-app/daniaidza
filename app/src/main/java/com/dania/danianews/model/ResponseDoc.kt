package com.dania.danianews.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseDoc(
    @SerializedName("docs")
    @Expose
    var docs: Array<Doc>? = null
)
