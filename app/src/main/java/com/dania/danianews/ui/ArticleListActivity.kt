package com.dania.danianews.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dania.danianews.MainRepository
import com.dania.danianews.MainViewModel
import com.dania.danianews.MyViewModelFactory
import com.dania.danianews.adapter.ArticleAdapter
import com.dania.danianews.adapter.SearchAdapter
import com.dania.danianews.api.RetrofitService
import com.dania.danianews.databinding.ActivityArticleListBinding

class ArticleListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityArticleListBinding
    private lateinit var viewModel: MainViewModel
    private val articleAdapter = ArticleAdapter()
    private val searchAdapter = SearchAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityArticleListBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        val retrofitService = RetrofitService.getInstance()
        val mainRepository = MainRepository(retrofitService)
        viewModel = ViewModelProvider(this, MyViewModelFactory(mainRepository)).get(MainViewModel::class.java)
        binding.title.text = intent.getStringExtra("title")

        viewModel.mostEmailList.observe(this) {
            it.results?.let { it1 -> articleAdapter.setArticleList(it1) }
        }

        viewModel.mostShareList.observe(this) {
            it.results?.let { it1 -> articleAdapter.setArticleList(it1) }
        }

        viewModel.mostViewList.observe(this) {
            it.results?.let { it1 -> articleAdapter.setArticleList(it1) }
        }

        viewModel.searchList.observe(this) {
            it.response?.docs?.let { it1 -> searchAdapter.setArticleList(it1) }
        }

        viewModel.errorMessage.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }

        viewModel.loading.observe(this, Observer {
            if (it) {
                binding.rlProgress.visibility = View.VISIBLE
            } else {
                binding.rlProgress.visibility = View.GONE
            }
        })

        binding.rvArticleList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        if (intent.getBooleanExtra("fromSearch", false)){
            binding.rvArticleList.adapter = searchAdapter
            viewModel.search(intent.getStringExtra("title")!!)
        } else {
            binding.rvArticleList.adapter = articleAdapter
            when {
                intent.getStringExtra("title")?.contains("Viewed") == true -> {
                    viewModel.getMostView()
                }
                intent.getStringExtra("title")?.contains("Shared") == true -> {
                    viewModel.getMostShare()
                }
                else -> {
                    viewModel.getMostEmail()
                }
            }
        }

        binding.ibBack.setOnClickListener {
            onBackPressed()
        }
    }
}