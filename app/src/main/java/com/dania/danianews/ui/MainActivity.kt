package com.dania.danianews.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dania.danianews.MainRepository
import com.dania.danianews.MainViewModel
import com.dania.danianews.MyViewModelFactory
import com.dania.danianews.R
import com.dania.danianews.adapter.MainAdapter
import com.dania.danianews.adapter.TopStoryAdapter
import com.dania.danianews.api.RetrofitService
import com.dania.danianews.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private val topStoryAdapter = TopStoryAdapter()
    private val mostViewAdapter = MainAdapter()
    private val mostShareAdapter = MainAdapter()
    private val mostEmailAdapter = MainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setOnClickListener()

        binding.rvTopStories.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.rvTopStories.adapter = topStoryAdapter
        binding.rvMostViewed.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.rvMostViewed.adapter = mostViewAdapter
        binding.rvMostShared.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.rvMostShared.adapter = mostShareAdapter
        binding.rvMostEmailed.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.rvMostEmailed.adapter = mostEmailAdapter

        val retrofitService = RetrofitService.getInstance()
        val mainRepository = MainRepository(retrofitService)

        viewModel = ViewModelProvider(this, MyViewModelFactory(mainRepository)).get(MainViewModel::class.java)

        viewModel.topStoryList.observe(this) {
            it.results?.let { it1 -> topStoryAdapter.setTopStories(it1) }
        }

        viewModel.mostViewList.observe(this) {
            it.results?.let { it1 -> mostViewAdapter.setMostList(it1) }
        }

        viewModel.mostShareList.observe(this) {
            it.results?.let { it1 -> mostShareAdapter.setMostList(it1) }
        }

        viewModel.mostEmailList.observe(this) {
            it.results?.let { it1 -> mostEmailAdapter.setMostList(it1) }
        }

        viewModel.errorMessage.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }

        viewModel.loading.observe(this, Observer {
            if (it) {
                binding.rlProgress.visibility = View.VISIBLE
            } else {
                binding.rlProgress.visibility = View.GONE
            }
        })

        viewModel.getTopStory()
        viewModel.getMostView()
        viewModel.getMostShare()
        viewModel.getMostEmail()
    }

    private fun setOnClickListener() {
        binding.moreEmailed.setOnClickListener {
            val intent = Intent(this, ArticleListActivity::class.java)
            intent.putExtra("title", getString(R.string.most_emailed))
            intent.putExtra("fromSearch", false)
            startActivity(intent)
        }
        binding.moreShared.setOnClickListener {
            val intent = Intent(this, ArticleListActivity::class.java)
            intent.putExtra("title", getString(R.string.most_shared))
            intent.putExtra("fromSearch", false)
            startActivity(intent)
        }
        binding.moreViewed.setOnClickListener {
            val intent = Intent(this, ArticleListActivity::class.java)
            intent.putExtra("title", getString(R.string.most_viewed))
            intent.putExtra("fromSearch", false)
            startActivity(intent)
        }
        binding.search.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }
}