package com.dania.danianews.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dania.danianews.databinding.ItemArticleBinding
import com.dania.danianews.model.Doc
import com.dania.danianews.utils.GlideApp

class SearchAdapter : RecyclerView.Adapter<SearchAdapterViewHolder>() {

    var articleList = mutableListOf<Doc>()

    fun setArticleList(movies: Array<Doc>) {
        this.articleList = movies.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchAdapterViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemArticleBinding.inflate(inflater, parent, false)
        return SearchAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchAdapterViewHolder, position: Int) {

        val result = articleList[position]
        holder.binding.newsTitle.text = result.headline?.main
        holder.binding.newsDate.text = result.pubDate
        if(result.multimedia!!.isNotEmpty())
            GlideApp.with(holder.itemView.context).load("https://static01.nyt.com/"+result.multimedia!![0].url).into(holder.binding.newsImage)

    }

    override fun getItemCount(): Int {
        return articleList.size
    }
}

class SearchAdapterViewHolder(val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root) {

}