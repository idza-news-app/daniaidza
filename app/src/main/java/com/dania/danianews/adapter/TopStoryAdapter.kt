package com.dania.danianews.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dania.danianews.databinding.ItemTopStoriesBinding
import com.dania.danianews.model.TopStoryResult
import com.dania.danianews.utils.GlideApp

class TopStoryAdapter: RecyclerView.Adapter<MainViewHolder>() {

    var topStoryList = mutableListOf<TopStoryResult>()

    fun setTopStories(movies: Array<TopStoryResult>) {
        this.topStoryList = movies.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemTopStoriesBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {

        val result = topStoryList[position]
        holder.binding.newsTitle.text = result.title
        if(result.multimedia!!.isNotEmpty())
            GlideApp.with(holder.itemView.context).load(result.multimedia!![0].url).into(holder.binding.newsImage)

    }

    override fun getItemCount(): Int {
        return topStoryList.size
    }
}

class MainViewHolder(val binding: ItemTopStoriesBinding) : RecyclerView.ViewHolder(binding.root) {

}