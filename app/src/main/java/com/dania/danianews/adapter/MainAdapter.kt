package com.dania.danianews.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dania.danianews.databinding.ItemMainArticleBinding
import com.dania.danianews.model.MostResult
import com.dania.danianews.utils.GlideApp

class MainAdapter: RecyclerView.Adapter<MainAdapterViewHolder>() {

    var mostList = mutableListOf<MostResult>()

    fun setMostList(movies: Array<MostResult>) {
        this.mostList = movies.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainAdapterViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMainArticleBinding.inflate(inflater, parent, false)
        return MainAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainAdapterViewHolder, position: Int) {

        val result = mostList[position]
        holder.binding.newsTitle.text = result.title
        if(result.media!!.isNotEmpty() && result.media!![0].mediaMetadata!!.isNotEmpty())
            GlideApp.with(holder.itemView.context).load(result.media!![0].mediaMetadata?.get(0)?.url).into(holder.binding.newsImage)

    }

    override fun getItemCount(): Int {
        return mostList.size
    }
}

class MainAdapterViewHolder(val binding: ItemMainArticleBinding) : RecyclerView.ViewHolder(binding.root) {

}