package com.dania.danianews.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dania.danianews.databinding.ItemArticleBinding
import com.dania.danianews.model.MostResult
import com.dania.danianews.utils.GlideApp

class ArticleAdapter: RecyclerView.Adapter<ArticleAdapterViewHolder>() {

    var articleList = mutableListOf<MostResult>()

    fun setArticleList(movies: Array<MostResult>) {
        this.articleList = movies.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleAdapterViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemArticleBinding.inflate(inflater, parent, false)
        return ArticleAdapterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleAdapterViewHolder, position: Int) {

        val result = articleList[position]
        holder.binding.newsTitle.text = result.title
        holder.binding.newsDate.text = result.published_date
        if(result.media!!.isNotEmpty() && result.media!![0].mediaMetadata!!.isNotEmpty())
            GlideApp.with(holder.itemView.context).load(result.media!![0].mediaMetadata?.get(0)?.url).into(holder.binding.newsImage)

    }

    override fun getItemCount(): Int {
        return articleList.size
    }
}

class ArticleAdapterViewHolder(val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root) {

}