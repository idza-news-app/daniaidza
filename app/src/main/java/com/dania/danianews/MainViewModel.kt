package com.dania.danianews

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dania.danianews.model.MostResponse
import com.dania.danianews.model.SearchResponse
import com.dania.danianews.model.TopStoriesResponse
import kotlinx.coroutines.*

class MainViewModel constructor(private val mainRepository: MainRepository) : ViewModel() {

    val errorMessage = MutableLiveData<String>()
    val topStoryList = MutableLiveData<TopStoriesResponse>()
    val mostViewList = MutableLiveData<MostResponse>()
    val mostShareList = MutableLiveData<MostResponse>()
    val mostEmailList = MutableLiveData<MostResponse>()
    val searchList = MutableLiveData<SearchResponse>()
    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }
    val loading = MutableLiveData<Boolean>()

    fun getTopStory() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.getTopStories()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    topStoryList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }

    }

    fun getMostView() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.getMostViews()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    mostViewList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }

    }

    fun getMostShare() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.getMostShares()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    mostShareList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }

    }

    fun getMostEmail() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.getMostEmails()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    mostEmailList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }

    }

    fun search(query: String) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = mainRepository.search(query)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    searchList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }

    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

}