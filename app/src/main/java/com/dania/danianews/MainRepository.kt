package com.dania.danianews

import com.dania.danianews.api.RetrofitService

class MainRepository constructor(private val retrofitService: RetrofitService) {
    private val X_API_KEY = "3GiNZ0LaHDCXPihw6R9PYyQnCQ647tE4"

    suspend fun getTopStories() = retrofitService.getTopStory(X_API_KEY)
    suspend fun getMostViews() = retrofitService.getMostViewed(X_API_KEY)
    suspend fun getMostShares() = retrofitService.getMostShared(X_API_KEY)
    suspend fun getMostEmails() = retrofitService.getMostEmailed(X_API_KEY)
    suspend fun search(query: String) = retrofitService.search(query,X_API_KEY)
}